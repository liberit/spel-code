                          about       SPEL        Idea

                            from   Logan    Streondj
                        with   license   CC-BY-SA       ya
                           time-at    March    6,  2015


Contents
1  Introduction                                                                1
2  Motivation                                                                  1

3  Ideal  Flow                                                                 2
4  Ideal  Pivot   Tree                                                         4

1     Introduction
about  ob contents
be description  and  illustration for plural idea  part of SPEL   ya
   about  for accessibility
su all description be  superset  of illustration yand
su all part of illustration be describe  in text ya


2     Motivation
about  now  problem
su many   programming     language   be  use  to program   all of computer  yand
su many   human    language   be  use  to speak  with  all of human   ya su most

                                         1


programming    language   be  design  for small  part  of computer    yand   su most
human   language   for small part of global  population    ya
   about   program   language  example
su C be  use for system   program   yand  su VHDL     for hardware    program   yand
su Javascript  for web  program  yand   su BASh    for shell program   yand   su Java
for portable program    yand  assembly   for fast program    yand  su  TeX  for print
document   program    yand  HTML     for web   document     yand   ChucK   for  audio
program   yand  SVG   for vector graphic   yand  OpenCl    for GPU    program   yand
su many   script for  small purpose   yand   su all of  them   be  have  ob diﬀerent
syntax  and  style of programming     yand  su  none  of  them  be  have  ob  human
language  syntax  ya
   about   SPEL   program   language   solution
be provide  ob  language  tha  be translate  to all other  ya
   about   human   language   example
su Mandarin   be native  language  for 955 million  people  or 14%   of earth human
yand  su Spanish   be  native  language  for  405  million  people   or 6%   of earth
human   yand  su  English  be native  language   for  360  million  people  or 5%  of
earth human   yand   su other  language  be  have  ob  less ya
   su  some  be assume   ob  tha su everyone   be  wil learn  ob English   but about
tha be learn ob  new  language  end-clause   be hard   yand  su it be  give ob unfair
advantage   to english  speaker  ya about   good   example    su Indonesia   be  past
have ob  majority   of Javanese  speaker   but  about   tha  to avoid  give  su them
ob advantage   end-clause   su they  be  choose   ob  trade  language   of Malay   as
oﬃcial  ya
   about   SPEL   human    language  solution
be provide  ob  language  tha  be translate  to all other  ya

3     Ideal     Flow
su input  agent be  have  ob idea ya
be write  ob idea  to natural  text by  using  word   deﬁnition   of input  language
tha from  SPEL   ya
be deconjugate   ob  natural  text to  analytic  input  text  by  using  conjugation
rules of input language   ya
su SPEL   be translate-in  ob  analytic input  text  to  mwak   pivot  tree by  using
word  translation  and  word  order of input  language    ya
be translate-out  ob  mwak   pivot  tree to  analytic  output   text  by  using word

                                         2


order and  word  translation  of output  language  ya
be conjugate  ob  analytic  output   text to conjugated text or source  code by
using conjugation  rules of output   language  ya
su output  agent  be compile   or understand   ob conjugated  or source code  to
understood  idea or native-code   by using output word deﬁnition or API  library
of output language   ya
su output  agent be  gain ob  skill or app  ya
   be see ob  ﬁgure  1 for illustration ya
























                                         3


oiunotpuputupucttoiunnwntdwbpoojeiewouuunrrradgttsopuadpntarunnwmbooudatbbttdareoeoltrbwenedyleddsdcaryetcraktaedoedntbtrsiotrtkiiﬁnrcceiastldeeundcalolancjianxeparodjuennownoiniustaatiﬁutegnsnjlrdvdiglriaualntaoopoojoiaapattugtrtinornwuttteetiepuagreotiisx-ontapootato-oontcrreiatnnntruunedtoeetdreetrmAxirluxvctiutwbPrlepetela-r-oeiIsccnalsreoolrsdidydlbaeoertraidoryner         4

                                                                                                                                             Figure                                                            1:                       about                                                       ideal                                            SPEL                           ﬂow  be illustration ya su green be Input Agent
                                                                                                                                             yand                                               su                        blue                                         be                          output                                                              Agent          ya su black  be provide by  SPEL  ya


4     Ideal     Pivot      Tree

su text be usually  have ob  title and  text array ya su text  array be able have
ob sentence  yand  be able  have  ob  subordinate  text or sentence  junction  ya
   su all junction be  have  ob head   and  body ya su all array  be able have  ob
plural member   ya
   su sentence  be usually  have  ob  sentence array  and  sentence particle yand
be able have  ob mood   ya su sentence   array be usally have  ob phrase  yand  be
able have  ob top clause  or phrase   junction ya
   su top  clause be usually  have   ob head  and subordinate   sentence  ya
   su phrase   be usually  have  ob  adposition  and type  yand  be  able have  ob
subordinate-clause  or type  junction   ya
   su subordinate-clause   be  usually  have  ob head  and  subordinate  sentence
ya
   su type  be  usually have  ob  body  yand  be able  have  ob body  classiﬁer or
genitive ya
   su genitive  be usually  have  ob  head  and subordinate   phrase ya
   be see  ob ﬁgure  2 for illustration  ya
















                                         5


                                          Text


                                    title       array


                         sub Text         Sentence             sentence  Junction


                           mood         array            sentence particle


        topClause            phrase Junction                           Phrase
6

  head          sub Sentence           adposition          Type           type Junction           Subclause


                                         classiﬁer         body          Genitive           head         sub Sentence


                                                                  head          sub Phrase
Figure  2: about ideal SPEL  Pivot Tree be illustration ya su dashed  line be show ob able have yand su full
line be show ob usually  have ya


