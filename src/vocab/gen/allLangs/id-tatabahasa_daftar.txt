ni	infinitif
na	nominatifkasus
ka	akusatifkasus
ta	temakasus
ca	Halo
wu	bentukpenyerukasus
kcah	lapar
ti	genitifkasus
ci	katasifat
wa	dan
mi	saya
sa	kamu
nyah	kami
ya	sayat
ma	tujuankasus
yi	datifkasus
li	realissuasanahati
tu	deontissuasanahati
cu	bersyaratsuasanahati
si	epistemiksuasanahati
yu	instrumentalkasus
nu	pedalamankonteks
pa	orangkonteks
tyah	klausaindependen
la	tergantungayat
fa	perfektifaspek
ki	lalutegang
ne	maskulinjeniskelamin
mwah	komitatifkasus
ra	menyajikantegang
pwah	benefaktifkasus
swih	essivekasus
no	ygmenolakpembilang
pi	permukaankonteks
da	adpositionalkasus
te	tempatkasus
to	ruangkonteks
ga	carakasus
ri	interogatifsuasanahati
ba	danatau
ku	eksklusif-atau
so	sumberkasus
pwih	ablatifkasus
ksah	kedekatan
tsah	demonstratif
ko	klausul-ekor
pyah	tapi
wi	universalpembilang
ji	wanitajeniskelamin
pu	jamakjumlah
bi	masadepantegang
co	sosialkonteks
me	tdksempurnaaspek
klah	sana
twah	relatifkasus
twih	terakhirkasus
di	direktifsuasanahati
tlah	fakultatifpembilang
kyah	apa
tcah	pertanyaankata
ja	katakerjapenghubung
cyah	orang
clah	lain
do	jumlah
tlih	tegaspembilang
se	waktukonteks
myah	sementarakasus
tyuh	hanya
tcih	ygmenegursuasanahati
tyih	retrospektifaspek
lo	volitivesuasanahati
cwah	setelah
nlah	alienablemilik
nlih	tidakdapatdicabutmilik
cyih	konsultatifsuasanahati
lwah	perlativekasus
pfih	progresifaspek
tsih	sini
ce	dibawahkonteks
pcah	juga
lwih	delativekasus
swah	semelfactiveaspek
nyih	neo
fu	fokus
plah	cinta
po	paucaljumlah
kyih	karena
ksih	kausalkasus
gi	nama
tfah	tepatkatabenda
crih	terlihatygberdasarataskenyataan
kceh	lebihbaik
dwih	Desiderativesuasanahati
wo	kata
srah	sampai
tweh	terminativekasus
nwih	inessivekasus
pcih	sangatpentingsuasanahati
crah	percaya
ro	negarakonteks
cwih	berharap
kwih	faktifygberdasarataskenyataan
fi	kembali
psih	optatifsuasanahati
tseh	potensisuasanahati
lweh	allativekasus
nrih	antropikjeniskelamin
tceh	samasubyekpenanda
re	berbedasubyek
croh	takut
mrah	memahami
cyuh	keraguan
twuh	dubitativesuasanahati
myih	indah
lyih	bunga
flah	kontrafaktabersyarat
we	vialiskasus
clih	dingin
pyih	perdamaian
su	kecuali
kwah	keinginan
le	panjangnya
bo	kemungkinan
mlah	kesulitan
syah	tersenyum
slih	tidur
zi	dikutip
twoh	quotativekasus
mo	inklusiforang
kcih	silahkan
rwih	precativesuasanahati
mrih	sopandaftar
rwah	bakalaspek
cyeh	heran
go	ygbersifatperibahasaaspek
tsoh	menghormati
za	inkoatifaspek
jwih	jussivesuasanahati
mwih	serbamembolehkansuasanahati
sloh	keberhasilan
prih	pribadi
de	distributifkasus
tcoh	rasasakit
ryah	tertawa
mlih	bijaksana
ye	posesifpenanda
ke	komisifsuasanahati
creh	sia-sia
mweh	ygmengakhiriaspek
nrah	hangat
nroh	marah
bwah	dengki
trah	kebanggaan
mroh	moral
slah	kesehatan
syih	sosial
pe	penghentianaspek
krih	ingintahu
tfih	intensifier
fe	akhirnya
prah	kesamping
trih	ygberdasarataskenyataan
nwah	ygberdasarataskenyataankasus
srih	sedih
zo	Zoicjeniskelamin
nweh	eventifsuasanahati
cwoh	samaperistiwapenanda
plih	perkaliankasus
dwah	diinginkan
kcoh	lelah
drah	puas
lu	spekulatifsuasanahati
frih	akrabdaftar
frah	luka
mruh	menipu
djih	simpati
krah	terima
cruh	malu
fyah	frasapenanda
ryih	ditakdirkan
kruh	pahit
tyeh	teks
kreh	kejam
tleh	hesternaltegang
kfah	mengorbankan
dyih	benci
cloh	rasionaljeniskelamin
nloh	menikmati
psah	putusasa
sleh	nabatijeniskelamin
nruh	tidakbersalah
drih	kekaguman
sroh	Maaf
brih	mendasarkan
kluh	kebingungan
bwih	doasuasanahati
klih	berdosa
grih	bodoh
nyeh	prakarsakasus
nleh	merendahkan
dlih	terlambatsangatpenting
proh	melaporkanygberdasarataskenyataan
mloh	melankolis
du	tunggal
jrih	kesabaran
freh	referensial
kloh	kesepian
kroh	pesona
tcuh	antusiasme
kcuh	induktifsuasanahati
xa	ceramahkonteks
nreh	gugup
pseh	aspek
ge	agen
mu	peringatansuasanahati
byah	ambisi
pcoh	ramai
treh	langsungygberdasarataskenyataan
yo	gelisahsuasanahati
cwuh	syok
tloh	barulalutegang
dlah	pastiartikel
cluh	jelek
myuh	kelucuan
dyah	distaldemonstratif
rwuh	cemburu
troh	ygmengutuksuasanahati
dzih	pengabaian
swuh	sombongsuasanahati
bzah	bersendagurau
gwih	yaitu
dloh	hariinitegang
dweh	delimitativeaspek
kweh	eksklusiforang
truh	suhu
tfeh	TELICaspek
lyeh	taktelisaspek
ru	terlalutinggisuasanahati
kfih	kekanak-kanakan
cyoh	mencurigakan
nluh	loyalitas
cweh	perangkap
nwoh	animasi
sruh	sensasi
ksoh	seksual
lyah	malas
mreh	momentaneaspek
pceh	menyesali
bjih	bekasmendiang
pcuh	ketidaktentuan
xi	biasaaspek
blih	dibatasi
tsuh	sepele
fwih	megah
grah	menyanjung
brah	sewenang-wenang
cleh	penuhkebajikan
sreh	resmidaftar
pfah	bermanfaat
droh	pendengaranygberdasarataskenyataan
flih	inferensialygberdasarataskenyataan
sweh	sublativekasus
ksuh	deklaratifsuasanahati
pleh	istimewa
psoh	takterkalahkan
be	lisan
mwoh	lamunan
kleh	keadaanygmenyedihkan
glah	keserakahan
kseh	setujusuasanahati
zrah	ketidakpuasan
dyoh	idiophone
syoh	terpesona
gwah	terlalubanyakkasus
zlih	irrealissuasanahati
zrih	perasaanmalu
byih	adaptasi
ryeh	katagantipenanda
fwah	hemat
bjah	gembiraluarbiasa
kxah	ketenangan
gvih	pegativekasus
pxih	pemulihannamabaik
xrah	berakhir
tfoh	untukselanjutnya
kxih	penyesalan
ryoh	prosecutivekasus
qyah	keasyikan
r6	partisip
txah	terpencillalutegang
xrih	bersejarahtegang
dxah	rangkapjumlah
syuh	super
vweh	terlalubanyakbekerja
n6	indrawiygberdasarataskenyataansuasanahati
bweh	obsesi
xlih	riang
txih	tusukan
sluh	puasdiri
txoh	gerakanmenanjak
xe	retrospektif
pxah	memikat
vwah	gerakanlereng
xyih	elite
fleh	infleksi
psuh	superessivekasus
syeh	satir
qi	transitif
xruh	gemetarketakutan
zwah	Wow
tr6h	gerakanhulu
qwah	tidakberambisi
xlah	diseluruhdunia
tyoh	intransitif
tluh	antiklimaks
qa	arushubunganpenanda
kxoh	anjinglelah
rwoh	urut
ve	vektor
kyoh	kilo
swoh	sekuensial
pxeh	ygdipisahkankasus
txeh	ygbertambah
dxih	predikatif
pxoh	prolativekasus
pfoh	profrasa
blah	bosan
fyih	nonmanusia
xo	aktorperan
pc6h	percobaanjumlah
greh	centrickasus
groh	tujuanpelatuk
dxoh	tengahsuara
rweh	miringkasus
xwah	sabarkasus
xu	prahariini
xroh	aus-out
ks6h	kausatif
vi	biner-nilai
dleh	modalkasus
gjeh	lain-jika
xreh	temansekerja
lwoh	lokatifkasus
ml6h	multaljumlah
gjih	galaksijeniskelamin
qlah	bintangjeniskelamin
bvih	abessivekasus
dvah	adessivekasus
je	agentifkasus
t6	agenpelatuk
kxeh	untukkasus
gxih	umumjeniskelamin
tc6h	kutipanuntukm
ts6h	transangka
ryuh	langsungobyek
gveh	ergatifkasus
pyoh	pasifsuara
qe	terbataskatakerja
xwih	utamaobyek
myoh	ygbersifatperibahasasuasanahati
vrih	terakhirpartikel
va	evitativekasus
bveh	subessivekasus
dxeh	kerasukankasus
dvih	mendapatkankursikasus
tsa7h	antessivekasus
dyeh	bersifatkataketerangankasus
mleh	menghidupkanjeniskelamin
pfeh	empatikegembiraan
xleh	kendaraanjeniskelamin
qlih	mineraljeniskelamin
xloh	lisankatabenda
dreh	keduabahasa
bvah	dirikepercayaan
qrah	perubahandariperistiwa
fo	berbedaperistiwa
sl6h	crastinaltegang
fyoh	refleksifsuara
bxih	alamsemestajeniskelamin
jlih	transitifkatakerja
kyeh	lokalitasjeniskelamin
txuh	penelitianjeniskelamin
s6	artistikjeniskelamin
preh	sabarpelatuk
sr6h	abstrakjeniskelamin
nyoh	tidaklangsungpidato
zreh	exocentrickasus
kfeh	laluperfektif
jo	adilkatakerja
kxuh	kolektifkatabenda
bu	absolutifkasus
qrih	planetjeniskelamin
kfoh	intibenefaktif
kwoh	asosiatifkasus
pluh	superlatifkasus
jrah	listrikarus
glih	matijeniskelamin
p6	propositivesuasanahati
vrah	timbal-baliksuara
lyuh	berturut-turutayat
gleh	segeramasadepantegang
lyoh	aplikatifsuara
ps6h	antipasifsuara
tfuh	continuativeaspek
nr6h	necessitativesuasanahati
pr6h	pasifpartisip
sy6h	eksistensialayat
qyih	bercahayaintensitas
gluh	singulativejumlah
kwuh	terlalubanyaklamanya
xyah	tunggaltindakankatakerja
vlih	superlatifgelar
djah	menyajikanpartisip
m6	jumlahdarizat
ti7	ikatpartikel
f6	epentheticmorfem
tli7h	terpencilmasadepantegang
si_	postpositionalkasus
l6	allocutionperjanjian
k6	ygberulang-ulangaspek
bzih	distributifkataganti
tu7	floating-point-nomor
vo	verba-akhiran
ki7	lalupasifpartisip
ta_	lokatifdirectionalkasus
